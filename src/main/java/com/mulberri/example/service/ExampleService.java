package com.mulberri.example.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.util.TableUtils;
import com.mulberri.example.dto.ExampleDTO;
import com.mulberri.example.dynamodb.entity.ExampleDynamodbEntity;

@Component
public class ExampleService {

	private DynamoDBMapper mapper;

	private static final Long READ_CAPACITY_UNITS = 1L;

	private static final Long WRITE_CAPACITY_UNITS = 1L;

	private static final ProvisionedThroughput THROUGHPUT = new ProvisionedThroughput(READ_CAPACITY_UNITS,
			WRITE_CAPACITY_UNITS);

	public ExampleService() {

//		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
		EndpointConfiguration endpointConfig = new EndpointConfiguration("http://localhost:8000", "us-west-2");
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withEndpointConfiguration(endpointConfig)
				.build();
		final CreateTableRequest createTableReq = createTableRequest(client, ExampleDynamodbEntity.class);
		TableUtils.createTableIfNotExists(client, createTableReq);
		mapper = new DynamoDBMapper(client);
	}

	private CreateTableRequest createTableRequest(AmazonDynamoDB dynamoDB, Class class1) {
		final DynamoDBMapper dynamoDBMapper = new DynamoDBMapper(dynamoDB);
		final CreateTableRequest createTableReq = dynamoDBMapper.generateCreateTableRequest(class1);
		createTableReq.setProvisionedThroughput(THROUGHPUT);
		return createTableReq;
	}

	public ExampleDTO getExample(String name) {
		ExampleDynamodbEntity hash = new ExampleDynamodbEntity();
		hash.setName(name);
		List<ExampleDynamodbEntity> entity = mapper.query(ExampleDynamodbEntity.class,
				new DynamoDBQueryExpression().withHashKeyValues(hash));
		ExampleDTO dto = new ExampleDTO();
		dto.setAge(entity.get(0).getAge());
		dto.setName(entity.get(0).getName());
		dto.setPosition("lala");
		return dto;
	}

	public void saveExample(ExampleDTO dto) {
		ExampleDynamodbEntity entity = new ExampleDynamodbEntity(dto.getName(), dto.getAge());
		mapper.save(entity);
	}
}
